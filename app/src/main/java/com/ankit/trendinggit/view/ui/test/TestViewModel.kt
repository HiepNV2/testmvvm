package com.ankit.trendinggit.view.ui.test

import androidx.lifecycle.MutableLiveData
import com.ankit.trendinggit.view.base.BaseViewModel

class TestViewModel : BaseViewModel() {
    val resultData = MutableLiveData<Int>()
    val arrData = MutableLiveData<ArrayList<Int>>()

    fun plusResult() {
        var resultValue = resultData.value ?: 0
        resultData.postValue(resultValue.plus(1))
    }

    fun minusResult() {
        var resultValue = resultData.value ?: 0
        resultData.postValue(resultValue.minus(1))
    }


}